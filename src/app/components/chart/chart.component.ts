import { Component, Input, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnChanges {
  @Input() weatherData: any;
  @Input() forecastType: any = 'Wind'; //default

  // lineChart
  public lineChartData: Array<any> = [];
  public lineChartLabels: Array<any> = [];
  public lineChartColors: Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: this.forecastType
        }
      }],
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Hours'
        }
      }]
    }
  };

  public lineChartLegend = true;
  public lineChartType = 'line';

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('weatherData') && changes.weatherData.currentValue) {
      this.setChartData('wind');
    }
    if (changes.hasOwnProperty('forecastType') && changes.forecastType.currentValue) {
      this.setChartData(changes.forecastType.currentValue);
    }
  }

  setChartData(forecastType: string) {
    const xAxis = [], yAxis = [];
    if (this.weatherData && this.weatherData.hasOwnProperty('forecast')) {
      for (const fc of this.weatherData.forecast) {
        xAxis.push(fc.hour);
        yAxis.push(forecastType === 'wind' ? fc.wind : fc.temp);
      }
      this.lineChartData[0] = {
        data: yAxis,
        label: forecastType
      };
      this.lineChartLabels = xAxis;
      this.lineChartOptions.scales.yAxes[0].scaleLabel.labelString = forecastType;
    }
  }

}
