import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WeatherService } from '@services/weather.service';

@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.scss']
})
export class CityListComponent implements OnInit {

  cities = null;

  constructor(private router: Router, private weather: WeatherService) {
    weather.getAllForecast().then(cities => {
      this.cities = cities;
    });
  }

  ngOnInit() {

  }

  showForecast(city) {
    setTimeout(() => {
      this.router.navigate(['/weather'], { queryParams: { city: city.code } });
    }, 500);
  }

}
