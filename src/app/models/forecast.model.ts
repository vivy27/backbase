export default class Forecast {
    days: Array<string> = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    day: string;
    hour: string;
    wind: number;
    temp: number;
    icon: string;
    condition: string;
    constructor(forecast: any, config: any) {
        this.day = this.getDay(forecast.dt_txt);
        this.hour = this.getTime(forecast.dt_txt);
        this.wind = Math.floor(forecast.wind.speed * 3.6);
        this.temp = forecast.main.temp;
        this.icon = `${config.openweather.icon}${forecast.weather[0].icon}.png`;
        this.condition = forecast.weather[0].description;
    }

    getTime(time: string) {
        const dt = new Date(time);
        const h = dt.getHours(), m = dt.getMinutes();
        const mm = (m < 10) ? `0${m}` : `${m}`;
        return (h > 12) ? (`${(h - 12)}:${mm} PM`) : (`${h}:${mm} AM`);
    }

    getDay(time: string) {
        const dt = new Date(time).getDay();
        return this.days[dt];
    }
}