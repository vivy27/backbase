
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from '@services/weather.service';
import { environment } from '@environments/environment';
import Forecast from '@models/forecast.model';

@Component({
  selector: 'app-weather-forecast',
  templateUrl: './weather-forecast.component.html',
  styleUrls: ['./weather-forecast.component.scss']
})
export class WeatherForecastComponent implements OnInit {

  private config = environment.weatherApi;
  public city = null;
  public weatherInfo = null;
  isTempChecked = false;

  constructor(private route: ActivatedRoute, private weather: WeatherService) { }

  ngOnInit() {

    if (this.route.snapshot.queryParams['city']) {
      this.city = this.route.snapshot.queryParams['city'];
    }

    this.weather.getCityForecast(this.city).then(data => {
      this.weatherInfo = this.formatData(data);
    });
  }

  formatData(data) {
    data.forecast = data.forecast.map(hourlyForecast => new Forecast(hourlyForecast, this.config));
    return data;
  }

}
