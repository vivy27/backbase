import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ChartsModule } from 'ng2-charts';
import { CityListComponent } from '@components/city-list/city-list.component';
import { CityWidgetComponent } from '@components/city-widget/city-widget.component';
import { WeatherForecastComponent } from '@components/weather-forecast/weather-forecast.component';
import { ChartComponent } from '@components/chart/chart.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: CityListComponent },
  { path: 'weather', component: WeatherForecastComponent }
];

@NgModule({
  declarations: [
    CityListComponent,
    CityWidgetComponent,
    WeatherForecastComponent,
    ChartComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
