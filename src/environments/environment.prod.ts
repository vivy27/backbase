export const environment = {
  production: true,
  weatherApi: {
    openweather: {
      host: 'https://api.openweathermap.org/data/2.5/',
      appid: '8cd37f9fdb4f8d117eae329ec175fa87',
      units: 'metric',
      icon: 'https://openweathermap.org/img/w/',
      weatherEndpoint: 'weather',
      forecastEndpoint: 'forecast'
    }
  }
};
