import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  private readonly configUrlPath: string = './configs/cities.config.json';
  private _config: any;
  get config() { return this._config; }

  constructor(private http: HttpClient) { }

  loader = () => {
    return this.http.get<any>(`${this.configUrlPath}`)
      .toPromise()
      .then(data => {
        this._config = data;
      });
  }
}
