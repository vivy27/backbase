import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from '@modules/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from '@components/root/app.component';
import { HeaderComponent } from '@components/header/header.component';
import { FooterComponent } from '@components/footer/footer.component';
import { ConfigurationService } from '@services/configuration.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [{
    provide: APP_INITIALIZER,
    useFactory: (configService: ConfigurationService) => configService.loader,
    deps: [ConfigurationService],
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
