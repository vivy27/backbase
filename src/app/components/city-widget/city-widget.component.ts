import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-city-widget',
  templateUrl: './city-widget.component.html',
  styleUrls: ['./city-widget.component.scss']
})
export class CityWidgetComponent implements OnInit {

  @Input() city: string;
  @Input() wind: number;
  @Input() temperature: number;

  constructor() { }

  ngOnInit() {
  }

}
