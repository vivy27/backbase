// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  weatherApi: {
    openweather: {
      host: 'https://api.openweathermap.org/data/2.5/',
      appid: '8cd37f9fdb4f8d117eae329ec175fa87',
      units: 'metric',
      icon: 'https://openweathermap.org/img/w/',
      weatherEndpoint: 'weather',
      forecastEndpoint: 'forecast'
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
