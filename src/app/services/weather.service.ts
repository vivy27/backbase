import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { ConfigurationService } from '@services/configuration.service';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  apiConfig = environment.weatherApi;
  cities = this.configService.config.cities;

  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAPIEndpoint(endpoint, city) {
    const service = this.apiConfig.openweather.host + this.apiConfig.openweather[endpoint];
    const qs = new URLSearchParams();
    qs.set('appid', this.apiConfig.openweather.appid);
    qs.set('units', this.apiConfig.openweather.units);
    qs.set('q', city);
    return `${service}?${qs}`;
  }

  async getAllForecast() {
    return await new Promise((resolveGet, rejectGet) => {
      const promises = this.cities.map((city) => (
        new Promise((resolve, reject) => {
          this.http.get(this.getAPIEndpoint('weatherEndpoint', city.code)).subscribe((res: any) => {
            city.wind = res.wind.speed;
            city.temp = res.main.temp;
            resolve(city);
          }, error => {
            console.log(error);
            reject(error);
          });
        })
      ));

      forkJoin(promises).subscribe(res => {
        resolveGet(res);
      }, error => {
        rejectGet(error);
      });

    });
  }

  async getCityForecast(city) {
    return await new Promise((resolve, reject) => {
      this.http.get(this.getAPIEndpoint('forecastEndpoint', city)).subscribe((res: any) => {
        resolve({
          city: res.city,
          forecast: res.list.slice(0, 4)
        });
      }, error => {
        reject(error);
      });
    });
  }
}
